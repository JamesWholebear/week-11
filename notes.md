# Notes

## Section 1

### app.js

- what is happening? Is this javascript? or html???
- well yes, but actually, no
- quackets (curly braces) are where things get real.
  - up to this point, it's just been HTML inside of JS
  - but now it's JS inside of JSX (or rather, JS inside of HTML inside of JS)

## Section 2

### breakdown.js 

- why are there no javascript errors?
- what is JSX?
- jsx -> js equivalent (nav)
- what is a component?
- How to think about a component at the basic level

## Section 3

### app2.js

- Bringing it all together
- Cards
- `title` prop (baking in goodies)
- using data to generate rapid content
- Javascript
  - Inside the Component
  - Inside of JSX

### index.js

- entry points
- /node_modules, `package.json` and `npm install`
- magic

## Bonus Talk

- Stateful things
- useState
- useEffect
