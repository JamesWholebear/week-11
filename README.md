Week 11 project

My notes are in `/notes.md`

Everything else is in `/src/`

To follow along, browse this repo with me or clone it and open it.

Cloning and running:
```
git clone https://gitlab.com/JamesWholebear/week-11.git

cd week-11/

npm install

npm run start
```