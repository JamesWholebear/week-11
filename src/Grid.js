const getGridStyles = columns => {
  return { 
    display: "grid", 
    gridTemplateColumns: "1fr ".repeat(columns), 
    gridRowGap: "1rem", 
    gridColumnGap: "1rem" 
  };
};

const ComponentWithInlineStylesLikeHtml = props => {
  return (
    <div style={{ backgroundColor: props.backgroundColor }}>{props.children}</div>
  );
}


const Grid = props => (
  <div style={ {...getGridStyles(props.columns, props.breakPoint), ...props.style} }>
    {props.children}
  </div>
);

export default Grid;