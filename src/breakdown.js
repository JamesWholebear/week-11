import React from "react";




































// so what _is_ JSX really?

const someJsx = <div>Hello there.</div>
// console.log(someJsx)


// you'll never do this, but this is the simplest way to look at it
export const SomeComponent = () => someJsx
export const SomeComponent2 = () => <div>Hello there.</div>
// console.log(SomeComponent2);

// could also do
function SomeComponent3() {
   return <div>General Kenobi.</div>
}



















































// how does that work? shouldn't it throw errors because of syntax? Yes, you're right, it should. But it doens't,
// because what you see isn't actually what the browser sees. Everyone's heard of a "compiler" by now, but you may not
// have heard of a "transpiler". Compilers take one language and transform it to a different one, like binary.
// Transpilers take one language and transform it back into the same language in a more browser readable way. Not all
// browsers are made equally, and some don't like es6 fat arrow functions, but none of them like JSX, so when we write
// JSX, it gets transpiled to an array of regular old javascript. 

// Before you look below, ask yourself the question, "what would it look like if I tracked _every_ aspect of my page in
// javascript and modified only the parts that change when a user interacts with it? Creating html elements in
// javascript, toggling classes, changing styles, removing divs, etc. You'd need a shorthand way to easily/quickly
// create/remove those html elements.





// create some new anchor via javascript
const newAnchor = document.createElement("a");

// give it some attributes
newAnchor.href = "#";
newAnchor.innerText = "click me";

// put it on the DOM

// document.body.appendChild(newAnchor);

// then you could have some event listener to listen when something updates, then check everywhere to see *if* 
// you should update

// important note: PLEASE don't do this in React because it goes against the declarative nature of React














// But alas, there's a better way 
// Here's an example (taken from https://www.reactenlightenment.com/react-jsx/5.1.html):

export var jsxNav = (
    <ul id="nav">
      <li><a href="#home">Home</a></li>
      <li><a href="#about">About</a></li>
      <li><a href="#clients">Clients</a></li>
      <li><a href="#contact">Contact Us</a></li>
    </ul>
);

// which is the same as: 

export var jsNav = React.createElement(
   "ul",
   { id: "nav" },
   React.createElement(
      "li",
      null,
      React.createElement(
         "a",
         { href: "#home" },
         "Home"
      )
   ),
   React.createElement(
      "li",
      null,
      React.createElement(
         "a",
         { href: "#about" },
         "About"
      )
   ),
   React.createElement(
      "li",
      null,
      React.createElement(
         "a",
         { href: "#clients" },
         "Clients"
      )
   ),
   React.createElement(
      "li",
      null,
      React.createElement(
         "a",
         { href: "#contact" },
         "Contact Us"
      )
   )
);

















// What is a component?

// A component is a function that returns JSX

// Just like there's many ways to make a function, there's that many ways (plus more) to make a React Component.
// You'll end up using _most_ of them, but each time you _need_ a component, different factors will determine what kind you need.


// Also, these are all the same, and these aren't "React" variants, it's just
// different ways to do things in Vanilla Javascript that React can do. Do what
// feels best for your current problem you're solving:
const FirstHello = () => <div>Hello</div>;

const SecondHello = props => <div>Hello</div>; // props are optional for when you need them

const ThirdHello = () => {
   // this is a component and I can put javascript in here! 
   
   return <div>Hello</div>;
}
const FourthHello = () => {
   // This is the most common and should probably use this as much as possible
   // until you're really comfortable with React. The extra parens around the
   // JSX keep it cleaner and easier to read
   return (
      <div>Hello</div>
   );
}

// This is the old way to do React. You'll only see this in legacy code. It does
// things differently than modern components. Don't forget to wash your hands
// after.
class FifthHello extends React.Component {
   render() {
      return (
         <div>Hello</div>
      );
   }
}


export const AllHello = () => (
   <div>
      <FirstHello />
      <SecondHello />
      <ThirdHello />
      <FourthHello />
      <FifthHello />
   </div>
);


































// How to think about a component at a basic level


export const ComponentThatRendersChildren = props => {
   return (
      <div>
         {props.children}
      </div>
   );
}

export const Div = props => {
   return (
      <div>
         {props.children}
      </div>
   );
}


// Back to App2.js